<?php

namespace Drupal\adc_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Block to display last visited pages you visited on the website.
 *
 * @Block(
 *   id = "adc_block_block",
 *   admin_label = @Translation("Analog Clock")
 * )
 */
class AnalogClockBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->configuration['num_places'] = 5;
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Grab the number of items to display.
    $layout = $this->configuration['layout'] ?? '';

    $timezone = $this->configuration['timezone'] ?? '';
    $content['heading'] = $this->configuration['heading'] ?? '';
    $content['footer'] = $this->configuration['footer'] ?? '';
    $layout_data['layout'] = $layout;

    switch ($timezone) {
      case 'system_timezone':
        $timezone = date_default_timezone_get();
        break;

      case 'local_timezone':
        $timezone = '';
        break;
    }
    $layout_data['timezone'] = $timezone;

    switch ($layout) {
      case 'layout1':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["hasShadow"] = FALSE;
        $layout_data["fillColor"] = "#292a2d";
        $layout_data["borderColor"] = "#ffffff";
        $layout_data["borderWidth"] = "1.5";
        $layout_data["fontColor"] = "#ffffff";
        $layout_data['fontWeight'] = "bold";
        $layout_data["pinColor"] = "#ffffff";
        $layout_data["majorTicksColor"] = "#ffffff";
        $layout_data["minorTicksColor"] = "#ffffff";
        $layout_data["hourHandColor"] = "#ffffff";
        $layout_data["minuteHandColor"] = "#ffffff";
        $layout_data["secondHandColor"] = "#ffa000";
        $layout_data["secondHandWidth"] = "1";
        $layout_data["secondHandLength"] = "90";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";

        break;

      case 'layout2':
        $layout_data["hasShadow"] = TRUE;
        $layout_data["shadowColor"] = "#a7a5a5";
        $layout_data['majorTicksWidth'] = "0.1";
        $layout_data['minorTicksWidth'] = "0.05";
        $layout_data['fillColor'] = "rgba(255, 255, 255, 0)";
        $layout_data['borderColor'] = "#fff";
        $layout_data['borderWidth'] = "3.0";
        $layout_data['fontColor'] = "#fff";
        $layout_data['fontWeight'] = "bold";
        $layout_data['pinColor'] = "#fff";
        $layout_data['majorTicksColor'] = "transparent";
        $layout_data['minorTicksColor'] = "transparent";
        $layout_data['hourHandColor'] = "#fff";
        $layout_data['minuteHandColor'] = "#fff";
        $layout_data['secondHandColor'] = "#fff";
        $layout_data['secondHandWidth'] = "2.0";
        $layout_data['secondHandLength'] = "70.0";
        $layout_data['minuteHandLength'] = "60.0";
        $layout_data['hourHandLength'] = "40.0";

        break;

      case 'layout3':
        $layout_data['majorTicksWidth'] = "0.1";
        $layout_data['minorTicksWidth'] = "0.05";
        $layout_data['fillColor'] = "#0007e0";
        $layout_data['borderColor'] = "#fff";
        $layout_data['borderWidth'] = "3.0";
        $layout_data['fontColor'] = "#fff";
        $layout_data['fontWeight'] = "bold";
        $layout_data['pinColor'] = "#fff";
        $layout_data['majorTicksColor'] = "transparent";
        $layout_data['minorTicksColor'] = "transparent";
        $layout_data['hourHandColor'] = "#fff";
        $layout_data['minuteHandColor'] = "#fff";
        $layout_data['secondHandColor'] = "#fff";
        $layout_data['secondHandWidth'] = "2.0";
        $layout_data['secondHandLength'] = "70.0";
        $layout_data['minuteHandLength'] = "60.0";
        $layout_data['hourHandLength'] = "40.0";
        break;

      case 'layout4':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["shadowColor"] = "#a7a5a5";
        $layout_data["hasShadow"] = TRUE;
        $layout_data["fillColor"] = "#ffffff";
        $layout_data["borderColor"] = "#000000";
        $layout_data["borderWidth"] = "2.5";
        $layout_data["fontColor"] = "#000000";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#ff0000";
        $layout_data["majorTicksColor"] = "#000000";
        $layout_data["minorTicksColor"] = "#000000";
        $layout_data["hourHandColor"] = "#000000";
        $layout_data["minuteHandColor"] = "#000000";
        $layout_data["secondHandColor"] = "#ff0044";
        $layout_data["secondHandWidth"] = "1";
        $layout_data["secondHandLength"] = "90";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";
        break;

      case 'layout5':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["shadowColor"] = "#a7a5a5";
        $layout_data["hasShadow"] = TRUE;
        $layout_data["fillColor"] = "#ffffff";
        $layout_data["borderColor"] = "#000000";
        $layout_data["borderWidth"] = "0.1";
        $layout_data["fontColor"] = "#000000";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#ff0000";
        $layout_data["majorTicksColor"] = "#000000";
        $layout_data["minorTicksColor"] = "#000000";
        $layout_data["hourHandColor"] = "#000000";
        $layout_data["minuteHandColor"] = "#000000";
        $layout_data["secondHandColor"] = "#ff0044";
        $layout_data["secondHandWidth"] = "1";
        $layout_data["secondHandLength"] = "90";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";
        break;

      case 'layout6':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["hasShadow"] = FALSE;
        $layout_data["fillColor"] = "#091921";
        $layout_data["borderColor"] = "#000000";
        $layout_data["borderWidth"] = "0.1";
        $layout_data["fontColor"] = "#ffffff";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#ffffff";
        $layout_data["majorTicksColor"] = "#ffffff";
        $layout_data["minorTicksColor"] = "#ffffff";
        $layout_data["hourHandColor"] = "#ff0000";
        $layout_data["minuteHandColor"] = "#ffffff";
        $layout_data["secondHandColor"] = "#ffffff";
        $layout_data["secondHandWidth"] = "1";
        $layout_data["secondHandLength"] = "90";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";
        break;

      case 'layout7':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["hasShadow"] = FALSE;
        $layout_data["fillColor"] = "#fcfcfc";
        $layout_data["borderColor"] = "#ff0000";
        $layout_data["borderWidth"] = "10";
        $layout_data["fontColor"] = "#ffffff";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#ff0000";
        $layout_data["majorTicksColor"] = "#ffffff";
        $layout_data["minorTicksColor"] = "#ffffff";
        $layout_data["hourHandColor"] = "#000000";
        $layout_data["minuteHandColor"] = "#000000";
        $layout_data["secondHandColor"] = "#ff0000";
        $layout_data["secondHandWidth"] = "3";
        $layout_data["minuteHandWidth"] = "4.0";
        $layout_data["hourHandWidth"] = "5.0";
        $layout_data["secondHandLength"] = "82";
        $layout_data["majorTicksLength"] = "10.0";
        $layout_data["minorTicksLength"] = "7.0";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";
        break;

      case 'layout8':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["hasShadow"] = FALSE;
        $layout_data["fillColor"] = "#fcfcfc";
        $layout_data["borderColor"] = "#6bcaef";
        $layout_data["borderWidth"] = "10";
        $layout_data["fontColor"] = "#424a4e";
        $layout_data["fontWeight"] = "900";
        $layout_data["pinColor"] = "#424a4e";
        $layout_data["majorTicksColor"] = "#424a4e";
        $layout_data["minorTicksColor"] = "#424a4e";
        $layout_data["hourHandColor"] = "#424a42";
        $layout_data["minuteHandColor"] = "#424a4e";
        $layout_data["secondHandColor"] = "#eba65c";
        $layout_data["secondHandWidth"] = "2";
        $layout_data["minuteHandWidth"] = "2.0";
        $layout_data["hourHandWidth"] = "3.0";
        $layout_data["secondHandLength"] = "70";
        $layout_data["majorTicksLength"] = "10.0";
        $layout_data["minorTicksLength"] = "7.0";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "15";

        break;

      case 'layout9':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["hasShadow"] = FALSE;
        $layout_data["fillColor"] = "#ffffff";
        $layout_data["borderColor"] = "#000000";
        $layout_data["borderWidth"] = "0.1";
        $layout_data["fontColor"] = "#424a4e";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#424a4e";
        $layout_data["majorTicksColor"] = "#424a4e";
        $layout_data["minorTicksColor"] = "#424a4e";
        $layout_data["hourHandColor"] = "#424a42";
        $layout_data["minuteHandColor"] = "#424a4e";
        $layout_data["secondHandColor"] = "#eba65c";
        $layout_data["secondHandWidth"] = "2";
        $layout_data["minuteHandWidth"] = "2.0";
        $layout_data["hourHandWidth"] = "3.0";
        $layout_data["secondHandLength"] = "70";
        $layout_data["majorTicksLength"] = "10.0";
        $layout_data["minorTicksLength"] = "7.0";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";

        break;

      case 'layout10':
        $layout_data["majorTicksWidth"] = "0.005";
        $layout_data["minorTicksWidth"] = "0.5";
        $layout_data["shadowColor"] = "#858585";
        $layout_data["hasShadow"] = TRUE;
        $layout_data["fillColor"] = "#ffffff";
        $layout_data["borderColor"] = "#000000";
        $layout_data["borderWidth"] = "3";
        $layout_data["fontColor"] = "#000000";
        $layout_data["fontWeight"] = "bolder";
        $layout_data["pinColor"] = "#000000";
        $layout_data["majorTicksColor"] = "#000000";
        $layout_data["minorTicksColor"] = "#000000";
        $layout_data["hourHandColor"] = "#000000";
        $layout_data["minuteHandColor"] = "#000000";
        $layout_data["secondHandColor"] = "#000000";
        $layout_data["secondHandWidth"] = "2";
        $layout_data["minuteHandWidth"] = "2.0";
        $layout_data["hourHandWidth"] = "3.0";
        $layout_data["secondHandLength"] = "80";
        $layout_data["majorTicksLength"] = "10.0";
        $layout_data["minorTicksLength"] = "7.0";
        $layout_data["minuteHandLength"] = "70.0";
        $layout_data["hourHandLength"] = "50.0";
        $layout_data["fontSize"] = "10";

        break;

      case 'custom':

        $layout_data['majorTicksWidth'] = $this->configuration['majorTicksWidth'] ?? '';
        $layout_data['minorTicksWidth'] = $this->configuration['minorTicksWidth'] ?? '';
        if (isset($this->configuration['hasShadow']) && $this->configuration['hasShadow']) {
          $layout_data['shadowColor'] = $this->configuration['shadowColor'] ?? '#000000';
          $layout_data['hasShadow'] = TRUE;
        }
        else {
          $layout_data['hasShadow'] = FALSE;
        }
        if (isset($this->configuration['fillColor_transparent']) && $this->configuration['fillColor_transparent'] == TRUE) {
          $layout_data['fillColor'] = 'transparent';
        }
        else {
          $layout_data['fillColor'] = $this->configuration['fillColor'] ?? '';
        }
        $layout_data['borderColor'] = $this->configuration['borderColor'] ?? '';
        $layout_data['borderWidth'] = $this->configuration['borderWidth'] ?? '';
        $layout_data['fontColor'] = $this->configuration['fontColor'] ?? '';
        $layout_data['fontWeight'] = $this->configuration['fontWeight'] ?? 'normal';
        $layout_data['pinColor'] = $this->configuration['pinColor'] ?? '';
        if (isset($this->configuration['minorTicksColor_transparent']) && $this->configuration['minorTicksColor_transparent'] == TRUE) {
          $layout_data['majorTicksColor'] = 'transparent';
        }
        else {
          $layout_data['majorTicksColor'] = $this->configuration['majorTicksColor'] ?? '';
        }
        if (isset($this->configuration['minorTicksColor_transparent']) && $this->configuration['minorTicksColor_transparent'] == TRUE) {
          $layout_data['minorTicksColor'] = 'transparent';
        }
        else {
          $layout_data['minorTicksColor'] = $this->configuration['minorTicksColor'] ?? '';
        }
        $layout_data['hourHandColor'] = $this->configuration['hourHandColor'] ?? '';
        $layout_data['minuteHandColor'] = $this->configuration['minuteHandColor'] ?? '';
        $layout_data['secondHandColor'] = $this->configuration['secondHandColor'] ?? '';
        $layout_data['secondHandWidth'] = $this->configuration['secondHandWidth'] ?? '';
        $layout_data['minuteHandWidth'] = $this->configuration['minuteHandWidth'] ?? '2.0';
        $layout_data['hourHandWidth'] = $this->configuration['hourHandWidth'] ?? '3.0';
        $layout_data['secondHandLength'] = $this->configuration['secondHandLength'] ?? '';
        $layout_data['majorTicksLength'] = $this->configuration['majorTicksLength'] ?? '10.0';
        $layout_data['minorTicksLength'] = $this->configuration['minorTicksLength'] ?? '7.0';
        $layout_data['minuteHandLength'] = $this->configuration['minuteHandLength'] ?? '';
        $layout_data['hourHandLength'] = $this->configuration['hourHandLength'] ?? '';
        $layout_data['fontSize'] = $this->configuration['fontSize'] ?? '';

        break;

      default:
        $layout_data['majorTicksWidth'] = "0.1";
        $layout_data['minorTicksWidth'] = "0.05";
        $layout_data['fillColor'] = "rgba(255, 255, 255, 0)";
        $layout_data['borderColor'] = "#fff";
        $layout_data['borderWidth'] = "3.0";
        $layout_data['fontColor'] = "#fff";
        $layout_data['fontWeight'] = "bold";
        $layout_data['pinColor'] = "#fff";
        $layout_data['majorTicksColor'] = "transparent";
        $layout_data['minorTicksColor'] = "transparent";
        $layout_data['hourHandColor'] = "#fff";
        $layout_data['minuteHandColor'] = "#fff";
        $layout_data['secondHandColor'] = "#fff";
        $layout_data['secondHandWidth'] = "2.0";
        $layout_data['secondHandLength'] = "70.0";
        $layout_data['minuteHandLength'] = "60.0";
        $layout_data['hourHandLength'] = "40.0";

        break;
    }

    $build = [];
    $build['clock'] = [
      '#theme' => 'analog_clock',
      '#data' => json_encode($layout_data),
      '#content' => $content,
    ];
    $build['#attached']['drupalSettings']['layout_data'] = $layout_data;
    $build['#attached']['library'][] = 'adc_block/adc_block.analog';
    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $local_timezone = [
      'system_timezone' => $this->t('System Timezone'),
      'local_timezone' => $this->t('Local Timezone'),
    ];
    $system_time_zones = TimeZoneFormHelper::getOptionsListByRegion();
    $timezones = $local_timezone + $system_time_zones;
    $form['regional_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Regional settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['regional_settings']['timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Default time zone'),
      '#default_value' => $config['timezone'] ?? '',
      '#options' => $timezones,
      '#attributes' => ['class' => ['timezone-detect']],
    ];
    $form['layout_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Layout settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $options = [
      'layout1' => 'Layout 1',
      'layout2' => 'Layout 2',
      'layout3' => 'Layout 3',
      'layout4' => 'Layout 4',
      'layout5' => 'Layout 5',
      'layout6' => 'Layout 6',
      'layout7' => 'Layout 7',
      'layout8' => 'Layout 8',
      'layout9' => 'Layout 9',
      'layout10' => 'Layout 10',
      'custom' => 'Custom Layout',
    ];
    $form['layout_settings']['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Layout'),
      '#default_value' => $config['layout'] ?? '',
      '#options' => $options,
      '#attributes' => ['class' => ['timezone-detect']],
    ];
    $form['layout_settings']['majorTicksWidth'] = [
      '#type' => 'number',
      '#step' => '.001',
      '#min' => '0',
      '#max' => '1',
      '#title' => $this->t('Major Ticks Width'),
      '#default_value' => $config['majorTicksWidth'] ?? '0.005',
      '#attributes' => ['class' => ['major-ticks-width']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Major tick width. eg 0.005'),
    ];
    $form['layout_settings']['minorTicksWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '0',
      '#max' => '0.5',
      '#title' => $this->t('Minor Ticks Width'),
      '#default_value' => $config['minorTicksWidth'] ?? '0.4',
      '#attributes' => ['class' => ['major-ticks-width']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Minor tick width. eg 0.0025'),
    ];
    $form['layout_settings']['minuteHandWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '5',
      '#title' => $this->t('Minute Hand Width'),
      '#default_value' => $config['minuteHandWidth'] ?? '2.0',
      '#attributes' => ['class' => ['major-ticks-width']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Minute Hand width. eg 2.0'),
    ];
    $form['layout_settings']['hourHandWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '10',
      '#title' => $this->t('Hour Hand Width'),
      '#default_value' => $config['hourHandWidth'] ?? '3.0',
      '#attributes' => ['class' => ['major-ticks-width']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Hour Hand width. eg 3.0'),
    ];
    $form['layout_settings']['hasShadow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clock Has Shadow'),
      '#default_value' => $config['hasShadow'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock has shadow'),
    ];
    $form['layout_settings']['shadowColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Clock Shadow Color'),
      '#default_value' => $config['shadowColor'] ?? '#000000',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][hasShadow]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Ticks Shadow color. for eg #000000'),
    ];
    $form['layout_settings']['fillColor_transparent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fill transparent'),
      '#default_value' => $config['fillColor_transparent'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Background transparent'),
    ];
    $form['layout_settings']['fillColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Clock Fill Color'),
      '#default_value' => $config['fillColor'] ?? '#333333',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][fillColor_transparent]"]' => ['checked' => FALSE],
        ],
      ],
      '#description' => $this->t('Clock Background color'),
    ];
    $form['layout_settings']['borderColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Clock Border Color'),
      '#default_value' => $config['borderColor'] ?? '#000000',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock Border color'),
    ];
    $form['layout_settings']['borderWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '0.1',
      '#max' => '15',
      '#title' => $this->t('Clock Border Width'),
      '#default_value' => $config['borderWidth'] ?? '2.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock Border width. eg 2'),
    ];
    $form['layout_settings']['fontColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Font Color'),
      '#default_value' => $config['fontColor'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock Hours Font color'),
    ];
    $form['layout_settings']['fontSize'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '10',
      '#max' => '35',
      '#title' => $this->t('font Size'),
      '#default_value' => $config['fontSize'] ?? '10.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock Font size. eg 10.0'),
    ];
    $form['layout_settings']['fontWeight'] = [
      '#type' => 'select',
      '#title' => $this->t('Font Weight'),
      '#default_value' => $config['fontWeight'] ?? 'normal',
      '#options' => [
        'normal' => $this->t('Normal'),
        'bold' => $this->t('Bold'),
        'bolder' => $this->t('Bolder'),
        'lighter' => $this->t('Lighter'),
        '100' => $this->t('100'),
        '200' => $this->t('200'),
        '300' => $this->t('300'),
        '400' => $this->t('400'),
        '500' => $this->t('500'),
        '600' => $this->t('600'),
        '700' => $this->t('700'),
        '800' => $this->t('800'),
        '900' => $this->t('900'),
      ],
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Clock Font size. eg 10.0'),
    ];
    $form['layout_settings']['pinColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Pin Color'),
      '#default_value' => $config['pinColor'] ?? '#ff8888',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Font color'),
    ];
    $form['layout_settings']['majorTicksColor_transparent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Major Ticks Color transparent'),
      '#default_value' => $config['majorTicksColor_transparent'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Hide Major Ticks'),
    ];
    $form['layout_settings']['majorTicksColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Major Ticks Color'),
      '#default_value' => $config['majorTicksColor'] ?? '#ff8888',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][majorTicksColor_transparent]"]' => ['checked' => FALSE],
        ],
      ],
      '#description' => $this->t('Major ticks color on the circle. For eg ticks on 12, 1, 2, 3 etc'),
    ];
    $form['layout_settings']['minorTicksColor_transparent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Minor Ticks Color Transparent'),
      '#default_value' => $config['minorTicksColor_transparent'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Hide Major Ticks'),
    ];
    $form['layout_settings']['minorTicksColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Minor Ticks Color'),
      '#default_value' => $config['minorTicksColor'] ?? '#ffaa00',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][minorTicksColor_transparent]"]' => ['checked' => FALSE],
        ],
      ],
      '#description' => $this->t('Minor ticks color on the circle. For eg. ticks between 12-1, 1-2, 2-3 etc'),
    ];
    $form['layout_settings']['hourHandColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Hour Hand Color'),
      '#default_value' => $config['hourHandColor'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Hour hand color'),
    ];
    $form['layout_settings']['minuteHandColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Minute Hand Color'),
      '#default_value' => $config['minuteHandColor'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Minute hand color'),
    ];
    $form['layout_settings']['secondHandColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Second Hand Color'),
      '#default_value' => $config['secondHandColor'] ?? '#ff0000',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Second hand color'),
    ];
    $form['layout_settings']['secondHandWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '15',
      '#title' => $this->t('Second Hand Width'),
      '#default_value' => $config['secondHandWidth'] ?? '1.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Second hand width. eg 1.0'),
    ];
    $form['layout_settings']['secondHandLength'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '50',
      '#max' => '100',
      '#title' => $this->t('Second Hand Length'),
      '#default_value' => $config['secondHandLength'] ?? '90.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Second hand width. eg 90.0'),
    ];
    $form['layout_settings']['minuteHandLength'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '50',
      '#max' => '100',
      '#title' => $this->t('Minute Hand Length'),
      '#default_value' => $config['minuteHandLength'] ?? '70.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Minute hand length. eg 70.0'),
    ];
    $form['layout_settings']['majorTicksLength'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '5',
      '#max' => '30',
      '#title' => $this->t('Major Ticks Length Length'),
      '#default_value' => $config['majorTicksLength'] ?? '10.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Major Ticks Length length. eg 10.0'),
    ];
    $form['layout_settings']['minorTicksLength'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '5',
      '#max' => '30',
      '#title' => $this->t('Minor Ticks Length Length'),
      '#default_value' => $config['minorTicksLength'] ?? '7.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Minor Ticks Length length. eg 7.0'),
    ];
    $form['layout_settings']['hourHandLength'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '50',
      '#max' => '100',
      '#title' => $this->t('Hour Hand Length'),
      '#default_value' => $config['hourHandLength'] ?? '50.0',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
      '#description' => $this->t('Hour hand length. eg 50.0'),
    ];
    $form['description_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Text Content settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['description_settings']['heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clock Heading'),
      '#default_value' => $config['heading'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
    ];
    $form['description_settings']['footer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clock Footer'),
      '#default_value' => $config['footer'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    $this->configuration['timezone'] = $form_state->getValue('regional_settings')['timezone'];
    $this->configuration['layout'] = $form_state->getValue('layout_settings')['layout'];

    $this->configuration['majorTicksWidth'] = $form_state->getValue('layout_settings')['majorTicksWidth'];
    $this->configuration['minorTicksWidth'] = $form_state->getValue('layout_settings')['minorTicksWidth'];
    $this->configuration['minuteHandWidth'] = $form_state->getValue('layout_settings')['minuteHandWidth'];
    $this->configuration['hourHandWidth'] = $form_state->getValue('layout_settings')['hourHandWidth'];
    $this->configuration['fillColor'] = $form_state->getValue('layout_settings')['fillColor'];
    $this->configuration['fillColor_transparent'] = $form_state->getValue('layout_settings')['fillColor_transparent'];

    $this->configuration['hasShadow'] = $form_state->getValue('layout_settings')['hasShadow'];
    $this->configuration['shadowColor'] = $form_state->getValue('layout_settings')['shadowColor'];
    $this->configuration['borderColor'] = $form_state->getValue('layout_settings')['borderColor'];
    $this->configuration['borderWidth'] = $form_state->getValue('layout_settings')['borderWidth'];
    $this->configuration['fontColor'] = $form_state->getValue('layout_settings')['fontColor'];
    $this->configuration['fontWeight'] = $form_state->getValue('layout_settings')['fontWeight'];
    $this->configuration['pinColor'] = $form_state->getValue('layout_settings')['pinColor'];
    $this->configuration['majorTicksColor'] = $form_state->getValue('layout_settings')['majorTicksColor'];
    $this->configuration['majorTicksColor_transparent'] = $form_state->getValue('layout_settings')['majorTicksColor_transparent'];
    $this->configuration['minorTicksColor_transparent'] = $form_state->getValue('layout_settings')['minorTicksColor_transparent'];
    $this->configuration['minorTicksColor'] = $form_state->getValue('layout_settings')['minorTicksColor'];
    $this->configuration['hourHandColor'] = $form_state->getValue('layout_settings')['hourHandColor'];
    $this->configuration['minuteHandColor'] = $form_state->getValue('layout_settings')['minuteHandColor'];
    $this->configuration['secondHandColor'] = $form_state->getValue('layout_settings')['secondHandColor'];
    $this->configuration['secondHandWidth'] = $form_state->getValue('layout_settings')['secondHandWidth'];
    $this->configuration['secondHandLength'] = $form_state->getValue('layout_settings')['secondHandLength'];
    $this->configuration['minuteHandLength'] = $form_state->getValue('layout_settings')['minuteHandLength'];
    $this->configuration['majorTicksLength'] = $form_state->getValue('layout_settings')['majorTicksLength'];
    $this->configuration['minorTicksLength'] = $form_state->getValue('layout_settings')['minorTicksLength'];
    $this->configuration['hourHandLength'] = $form_state->getValue('layout_settings')['hourHandLength'];
    $this->configuration['fontSize'] = $form_state->getValue('layout_settings')['fontSize'];
    $this->configuration['heading'] = $form_state->getValue('description_settings')['heading'];
    $this->configuration['footer'] = $form_state->getValue('description_settings')['footer'];
  }

  /**
   * {@inheritdoc}
   *
   * Disable the block cache to keep the Analog Clock block updated.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
