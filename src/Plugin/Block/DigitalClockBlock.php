<?php

namespace Drupal\adc_block\Plugin\Block;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Block to display last visited pages you visited on the website.
 *
 * @Block(
 *   id = "adc_block_digital_block",
 *   admin_label = @Translation("Digital Clock")
 * )
 */
class DigitalClockBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a DigitalClockBlock object.
   *
   * @param array $configuration
   *   A configuration array containing configuration settings for the block.
   * @param string $plugin_id
   *   The plugin id for the block.
   * @param array $plugin_definition
   *   The configuration for the plugin.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager, ?TimeInterface $time = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->configuration['num_places'] = 5;
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $date_format = $this->configuration['date_format'] ?? 'medium';

    $timezone = $this->configuration['timezone'] ?? '';
    $show_date = $this->configuration['show_date'] ?? FALSE;
    $layout = $this->configuration['layout'] ?? '';
    $content['heading'] = $this->configuration['heading'] ?? '';
    $content['footer'] = $this->configuration['footer'] ?? '';

    $current_date = '';

    if ($show_date) {
      $date_format = $this->configuration['date_format'] ?? 'medium';
      if ($date_format == 'custom') {
        $custom_fromat = TRUE;
        $date_format = $this->configuration['custom_date_format'] ?? 'medium';
      }
      $current_timestamp = time();

      if ($custom_fromat) {
        $current_date = $this->dateFormatter->format($current_timestamp, 'custom', $date_format);
      }
      else {
        $current_date = $this->dateFormatter->format($current_timestamp, $date_format);
      }
    }

    switch ($timezone) {
      case 'system_timezone':
        $timezone = date_default_timezone_get();
        break;

      case 'local_timezone':
        $timezone = '';
        break;
    }

    $data['timezone'] = $timezone;

    switch ($layout) {
      case 'layout1':
        $data['current_date'] = '';
        $data['layout'] = 'custom';
        $data['show_date'] = 0;
        $data['date_format'] = 'html_month';
        $data['custom_date_format'] = '';
        $data['container_backgraound_color'] = '#ffffff';
        $data['container_box_shadow_enable'] = 0;
        $data['container_box_shadow'] = '#c4c4c4';
        $data['time_color'] = '#000000';
        $data['date_color'] = '#fbc1c1';
        $data['description_color'] = '#850000';
        $data['time_text_shadow'] = '#00d68f';
        $data['date_text_shadow'] = '#ffae00';
        $data['description_text_shadow'] = '#c54a07';
        $data['time_font_size'] = '50';
        $data['date_font_size'] = '50';
        $data['description_font_size'] = '42';
        $data['description_text'] = '';
        $data['time_text_shadow_enable'] = 0;
        $data['date_text_shadow_enable'] = 0;
        break;

      case 'custom':
        $data['current_date'] = $current_date;

        $data['layout'] = $this->configuration['layout'] ?? '';
        $data['show_date'] = $show_date;
        $data['layout'] = $this->configuration['layout'] ?? '';
        $data['date_format'] = $this->configuration['date_format'] ?? '';
        $data['custom_date_format'] = $this->configuration['custom_date_format'] ?? '';
        $data['container_backgraound_color'] = $this->configuration['container_backgraound_color'] ?? '';
        $data['container_box_shadow_enable'] = $this->configuration['container_box_shadow_enable'] ?? '';
        $data['container_box_shadow'] = $this->configuration['container_box_shadow'] ?? '#000';
        $data['time_color'] = $this->configuration['time_color'] ?? '';
        $data['date_color'] = $this->configuration['date_color'] ?? '';
        $data['description_color'] = $this->configuration['description_color'] ?? '';
        $data['time_text_shadow'] = $this->configuration['time_text_shadow'] ?? '';
        $data['date_text_shadow'] = $this->configuration['date_text_shadow'] ?? '';
        $data['description_text_shadow'] = $this->configuration['description_text_shadow'] ?? '';
        $data['time_font_size'] = $this->configuration['time_font_size'] ?? '';
        $data['date_font_size'] = $this->configuration['date_font_size'] ?? '';
        $data['description_font_size'] = $this->configuration['description_font_size'] ?? '';
        $data['description_text'] = $this->configuration['description_text'] ?? '';
        $data['time_text_shadow_enable'] = $this->configuration['time_text_shadow_enable'] ?? FALSE;

        $data['date_text_shadow_enable'] = $this->configuration['date_text_shadow_enable'] ?? FALSE;
        $data['description_text_shadow_enable'] = $this->configuration['description_text_shadow_enable'] ?? FALSE;

        break;

      default:
        $data['current_date'] = '';
        $data['layout'] = 'custom';
        $data['show_date'] = 0;
        $data['date_format'] = 'html_month';
        $data['custom_date_format'] = '';
        $data['container_backgraound_color'] = '#ffffff';
        $data['container_box_shadow_enable'] = 0;
        $data['container_box_shadow'] = '#c4c4c4';
        $data['time_color'] = '#000000';
        $data['date_color'] = '#fbc1c1';
        $data['description_color'] = $this->configuration['description_color'] ?? '';
        $data['time_text_shadow'] = '#00d68f';
        $data['description_text_shadow'] = $this->configuration['description_text_shadow'] ?? '';
        $data['time_font_size'] = '50';
        $data['date_font_size'] = '50';
        $data['date_text_shadow'] = $this->configuration['date_text_shadow'] ?? '';
        $data['description_font_size'] = $this->configuration['description_font_size'] ?? '';
        $data['description_text'] = $this->configuration['description_text'] ?? '';
        $data['time_text_shadow_enable'] = FALSE;
        $data['date_text_shadow_enable'] = 0;
        $data['description_text_shadow_enable'] = $this->configuration['description_text_shadow_enable'] ?? FALSE;

        break;
    }

    return [
      '#theme' => 'digital_clock',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'adc_block/adc_block.digital',
        ],
        'drupalSettings' => [
          'config_data' => [],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();

    $local_timezone = [
      'system_timezone' => $this->t('System Timezone'),
    ];
    $system_time_zones = TimeZoneFormHelper::getOptionsListByRegion();
    $timezones = $local_timezone + $system_time_zones;

    $form['regional_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Regional settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['regional_settings']['timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Default time zone'),
      '#default_value' => $config['timezone'] ?? '',
      '#options' => $timezones,
      '#attributes' => ['class' => ['timezone-detect']],
    ];
    $form['layout_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Layout settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $options = [
      'layout1' => 'Layout 1',
      'custom' => 'Custom Layout',
    ];
    $form['layout_settings']['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Layout'),
      '#default_value' => $config['layout'] ?? '',
      '#options' => $options,
      '#attributes' => ['class' => ['timezone-detect']],
    ];

    $form['layout_settings']['show_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Date'),
      '#default_value' => $config['show_date'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $date_formats = [];

    $formats = $this->entityTypeManager->getStorage('date_format')->loadMultiple();

    foreach ($formats as $machine_name => $value) {
      $date_formats[$machine_name] = $this->t(
        '@name format: @date',
        [
          '@name' => $value->label(),
          '@date' => $this->dateFormatter->format($this->time->getRequestTime(), $machine_name),
        ]
      );
    }

    $date_formats['custom'] = $this->t('Custom');

    $form['layout_settings']['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $date_formats,
      '#default_value' => $this->configuration['date_format'] ?? 'medium',
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][show_date]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['layout_settings']['custom_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom date format'),
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->configuration['custom_date_format'] ?? '',
    ];

    $form['layout_settings']['custom_date_format']['#states']['visible'][] = [
      ':input[name="settings[layout_settings][date_format]"]' => ['value' => 'custom'],
    ];

    $form['layout_settings']['container_backgraound_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Container Backgraound Color'),
      '#default_value' => $config['container_backgraound_color'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['layout_settings']['container_box_shadow_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Container Box Shadow'),
      '#default_value' => $config['container_box_shadow_enable'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['layout_settings']['container_box_shadow'] = [
      '#type' => 'color',
      '#title' => $this->t('Container box shadow color'),
      '#default_value' => $config['container_box_shadow'] ?? '#000000',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][container_box_shadow_enable]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Container box shadow color'),
    ];

    $form['layout_settings']['time_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Time Color'),
      '#default_value' => $config['time_color'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['layout_settings']['date_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Date Color'),
      '#default_value' => $config['date_color'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['layout_settings']['time_text_shadow_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Time Shadow'),
      '#default_value' => $config['time_text_shadow_enable'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['layout_settings']['time_text_shadow'] = [
      '#type' => 'color',
      '#title' => $this->t('Time Text Shadow'),
      '#default_value' => $config['time_text_shadow'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][time_text_shadow_enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['layout_settings']['date_text_shadow_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Text Shadow'),
      '#default_value' => $config['date_text_shadow_enable'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['layout_settings']['date_text_shadow'] = [
      '#type' => 'color',
      '#title' => $this->t('Date Text Shadow'),
      '#default_value' => $config['date_text_shadow'] ?? '#ffffff',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
          ':input[name="settings[layout_settings][date_text_shadow_enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['layout_settings']['time_font_size'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '100',
      '#title' => $this->t('Time Font Size'),
      '#default_value' => $config['time_font_size'] ?? '24',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['layout_settings']['date_font_size'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '100',
      '#title' => $this->t('Date Font Size'),
      '#default_value' => $config['date_font_size'] ?? '26',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[layout_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['description_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Description settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['description_settings']['description_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description Text'),
      '#default_value' => $config['description_text'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
    ];

    $form['description_settings']['description_font_size'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#min' => '1',
      '#max' => '100',
      '#title' => $this->t('Description Font Size'),
      '#default_value' => $config['description_font_size'] ?? '25',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[description_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['description_settings']['description_text_shadow_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Description Text Shadow'),
      '#default_value' => $config['description_text_shadow_enable'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[description_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['description_settings']['description_text_shadow'] = [
      '#type' => 'color',
      '#title' => $this->t('Description Text Shadow'),
      '#default_value' => $config['description_text_shadow'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[description_settings][description_text_shadow_enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['description_settings']['description_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Description Color'),
      '#default_value' => $config['description_color'] ?? '',
      '#attributes' => ['class' => ['timezone-detect']],
      '#states' => [
        'visible' => [
          ':input[name="settings[description_settings][layout]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['timezone'] = $form_state->getValue('regional_settings')['timezone'];
    $this->configuration['layout'] = $form_state->getValue('layout_settings')['layout'];
    $this->configuration['show_date'] = $form_state->getValue('layout_settings')['show_date'];
    $this->configuration['layout'] = $form_state->getValue('layout_settings')['layout'];
    $this->configuration['date_format'] = $form_state->getValue('layout_settings')['date_format'];
    $this->configuration['custom_date_format'] = $form_state->getValue('layout_settings')['custom_date_format'];
    $this->configuration['container_backgraound_color'] = $form_state->getValue('layout_settings')['container_backgraound_color'];
    $this->configuration['container_box_shadow_enable'] = $form_state->getValue('layout_settings')['container_box_shadow_enable'];
    $this->configuration['container_box_shadow'] = $form_state->getValue('layout_settings')['container_box_shadow'];
    $this->configuration['time_color'] = $form_state->getValue('layout_settings')['time_color'];
    $this->configuration['date_color'] = $form_state->getValue('layout_settings')['date_color'];
    $this->configuration['description_color'] = $form_state->getValue('description_settings')['description_color'];
    $this->configuration['time_text_shadow'] = $form_state->getValue('layout_settings')['time_text_shadow'];
    $this->configuration['date_text_shadow'] = $form_state->getValue('layout_settings')['date_text_shadow'];
    $this->configuration['description_text_shadow'] = $form_state->getValue('description_settings')['description_text_shadow'];
    $this->configuration['time_text_shadow_enable'] = $form_state->getValue('layout_settings')['time_text_shadow_enable'];
    $this->configuration['date_text_shadow_enable'] = $form_state->getValue('layout_settings')['date_text_shadow_enable'];
    $this->configuration['description_text_shadow_enable'] = $form_state->getValue('description_settings')['description_text_shadow_enable'];
    $this->configuration['time_font_size'] = $form_state->getValue('layout_settings')['time_font_size'];
    $this->configuration['date_font_size'] = $form_state->getValue('layout_settings')['date_font_size'];
    $this->configuration['description_font_size'] = $form_state->getValue('description_settings')['description_font_size'];
    $this->configuration['description_text'] = $form_state->getValue('description_settings')['description_text'];
  }

  /**
   * {@inheritdoc}
   *
   * Disable block cache to keep it the Analog Clock update.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
