(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.digital = {
    attach: function (context, settings) {
      var changeTimezone = function (date, ianatz) {
        var invdate = new Date(date.toLocaleString('en-US', {
          timeZone: ianatz
        }));
        var diff = date.getTime() - invdate.getTime();
        return new Date(date.getTime() - diff);
      };
      setInterval(() => {
        $(".adc_block-digitaltime").filter(function () {
          let timezone = $(this).attr('data-timezone');
          let time = new Date();
          if (jQuery(this)[0].hasAttribute('data-timezone')) {
            let timezone = $(this).attr('data-timezone');
            time = changeTimezone(time, timezone);
          }
          let hours = time.getHours();
          let minutes = time.getMinutes();
          let seconds = time.getSeconds();
          let amPm = "";
          if (hours > 12) {
            hours = hours - 12;
            amPm = "PM";
          } else if (hours == 0) {
            hours = 12;
            amPm = "AM";
          } else {
            amPm = "AM";
          }
          hours = hours >= 10 ? hours : "0" + hours;
          minutes = minutes >= 10 ? minutes : "0" + minutes;
          seconds = seconds >= 10 ? seconds : "0" + seconds;
          var clock_data = `${hours}:${minutes}:${seconds} ${amPm}`;
          $(this).html(clock_data);
        });
      }, 1000);
    }
  }
}(jQuery, Drupal, drupalSettings));
