(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.analog_behavior = {
    attach: function (context, settings) {
      (function ($) {
        function core(id, preset, options) {
          var adc_blockcanvas = $(id)[0];
          var ctx = adc_blockcanvas.getContext('2d');
          var adc_blockbound = adc_blockcanvas.height;
          var adc_blocksafepad = 0;
          if (preset.hasShadow) {
            adc_blocksafepad = 4;
          }
          var adc_blockradius = adc_blockcanvas.height / 2 - adc_blocksafepad;
          var adc_blocksecondStep = 2 * Math.PI / 60;
          var adc_blockhourStep = 2 * Math.PI / 12;
          var adc_blockinitialize = function () {
            $(adc_blockcanvas).css('max-width', '100%');
            $(adc_blockcanvas).css('width', $(adc_blockcanvas).css('height'));
            adc_blockcanvas.width = adc_blockcanvas.height;
            if (preset.hasShadow) {
              ctx.shadowOffsetX = 0.0;
              ctx.shadowOffsetY = 0.0;
              ctx.shadowBlur = preset.shadowBlur;
              ctx.shadowColor = preset.shadowColor;
            }
            draw();
          };
          var adc_blockp2v = function (value) {
            return value / 100.0 * adc_blockradius;
          };
          var adc_blockdrawMajorLines = function () {
            ctx.lineWidth = adc_blockp2v(preset.majorTicksLength);
            ctx.strokeStyle = preset.majorTicksColor;
            for (var i = 1; i <= 12; i++) {
              ctx.beginPath();
              ctx.arc(adc_blockradius + adc_blocksafepad, adc_blockradius + adc_blocksafepad, adc_blockradius - ctx.lineWidth / 1.9, i * adc_blockhourStep - adc_blockp2v(preset.majorTicksWidth) / 2, i * adc_blockhourStep + adc_blockp2v(preset.majorTicksWidth) / 2);
              ctx.stroke();
            }
          };
          var adc_blockdrawMinorLines = function () {
            ctx.lineWidth = adc_blockp2v(preset.minorTicksLength);
            ctx.strokeStyle = preset.minorTicksColor;
            var secHandLength = 96 * 2;
            for (var i = 0; i < 60; i++) {
              angle = (i - 3) * (Math.PI * 2) / 60;
              ctx.lineWidth = adc_blockp2v(preset.minorTicksWidth);
              ctx.beginPath();
              var x1 = (adc_blockcanvas.width / 2) + Math.cos(angle) * (secHandLength);
              var y1 = (adc_blockcanvas.height / 2) + Math.sin(angle) * (secHandLength);
              var x2 = (adc_blockcanvas.width / 2) + Math.cos(angle) * (secHandLength - (secHandLength / 30));
              var y2 = (adc_blockcanvas.height / 2) + Math.sin(angle) * (secHandLength - (secHandLength / 30));
              ctx.moveTo(x1, y1);
              ctx.lineTo(x2, y2);
              ctx.strokeStyle = ctx.strokeStyle;
              ctx.stroke();
            }
          };
          var drawBorder = function () {
            ctx.strokeStyle = preset.borderColor;
            ctx.lineWidth = adc_blockp2v(preset.borderWidth);
            ctx.beginPath();
            ctx.arc(adc_blockradius + adc_blocksafepad, adc_blockradius + adc_blocksafepad, adc_blockradius - ctx.lineWidth / 2, 0, 2 * Math.PI);
            ctx.stroke();
          };
          var drawFill = function () {
            ctx.fillStyle = preset.fillColor;
            ctx.lineWidth = adc_blockp2v(preset.borderWidth);
            ctx.beginPath();
            ctx.arc(adc_blockradius + adc_blocksafepad, adc_blockradius + adc_blocksafepad, adc_blockradius - ctx.lineWidth, 0.0, 2 * Math.PI);
            ctx.fill();
          };
          var drawHandle = function (angle, lengthPercent, widthPercent, color) {
            var x = angle - Math.PI / 2;
            x = Math.cos(x) * adc_blockp2v(lengthPercent);
            var x_1 = angle - Math.PI / 2;
            var y = Math.sin(x_1) * adc_blockp2v(lengthPercent);
            ctx.lineWidth = adc_blockp2v(widthPercent);
            ctx.strokeStyle = color;
            ctx.beginPath();
            ctx.moveTo(adc_blockradius + adc_blocksafepad, adc_blockradius + adc_blocksafepad);
            ctx.lineTo(adc_blockradius + adc_blocksafepad + x, adc_blockradius + adc_blocksafepad + y);
            ctx.stroke();
          };
          var drawTexts = function () {
            if (typeof preset.fontWeight === 'undefined') {
              var fontWeight = 'normal'
            } else {
              var fontWeight = preset.fontWeight;
            }
            for (var i = 1; i <= 12; i++) {
              var angle = i * adc_blockhourStep;
              var x = angle - Math.PI / 2;
              x = Math.cos(x) * adc_blockp2v(80.0);
              var x_1 = angle - Math.PI / 2;
              var y = Math.sin(x_1) * adc_blockp2v(80.0);
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              ctx.font = fontWeight + ' ' + adc_blockp2v(preset.fontSize).toString() + 'px ' + preset.fontName;
              ctx.fillStyle = preset.fontColor;
              ctx.beginPath();
              ctx.fillText(i.toString(), adc_blockradius + adc_blocksafepad + x, adc_blockradius + adc_blocksafepad + y);
              ctx.stroke();
            }
          };
          var drawPin = function () {
            ctx.fillStyle = preset.pinColor;
            ctx.beginPath();
            ctx.arc(adc_blockradius + adc_blocksafepad, adc_blockradius + adc_blocksafepad, adc_blockp2v(preset.pinRadius), 0.0, 2 * Math.PI);
            ctx.fill();
          };
          var changeTimezone = function (date, ianatz) {
            var invdate = new Date(date.toLocaleString('en-US', {
              timeZone: ianatz
            }));
            var diff = date.getTime() - invdate.getTime();
            return new Date(date.getTime() - diff);
          };
          var draw = function () {
            ctx.clearRect(0.0, 0.0, adc_blockbound, adc_blockbound);
            ctx.lineCap = 'butt';
            if (preset.drawFill) {
              drawFill();
            }
            if (preset.drawMinorTicks) {
              adc_blockdrawMinorLines();
            }
            if (preset.drawMajorTicks) {
              adc_blockdrawMajorLines();
            }
            if (preset.drawBorder) {
              drawBorder();
            }
            if (preset.drawTexts) {
              drawTexts();
            }
            var date = new Date();
            if (options.timezone) {
              date = changeTimezone(date, options.timezone);
            }
            var s = date.getSeconds();
            var m = date.getMinutes();
            var h = date.getHours();
            m += s / 60.0;
            h += m / 60.0;
            ctx.lineCap = 'round';
            drawHandle(h * adc_blockhourStep, preset.hourHandLength, preset.hourHandWidth, preset.hourHandColor);
            drawHandle(m * adc_blocksecondStep, preset.minuteHandLength, preset.minuteHandWidth, preset.minuteHandColor);
            if (preset.drawSecondHand) {
              drawHandle(s * adc_blocksecondStep, preset.secondHandLength, preset.secondHandWidth, preset.secondHandColor);
            }
            if (preset.drawPin) {
              drawPin();
            }
            window.requestAnimationFrame(function () {
              draw(this);
            });
          };
          adc_blockinitialize();
        }
        $.fn.htAnalogClock = function (preset, options) {
          return this.each(function () {
            var _preset = $.extend({}, htAnalogClock.preset_default, preset || {});
            var _options = $.extend({}, $.fn.htAnalogClock.defaultOptions, options || {});
            core(this, _preset, _options);
          });
        };
        $.fn.htAnalogClock.defaultOptions = {
          timezone: "NULL"
        };
      }(jQuery));

      function htAnalogClock() {}
      htAnalogClock.preset_default = {
        hasShadow: "TRUE",
        shadowColor: "#000",
        shadowBlur: 10,
        drawSecondHand: "TRUE",
        drawMajorTicks: "TRUE",
        drawMinorTicks: "TRUE",
        drawBorder: "TRUE",
        drawFill: "TRUE",
        drawTexts: "TRUE",
        drawPin: "TRUE",
        majorTicksColor: "#f88",
        minorTicksColor: "#fa0",
        majorTicksLength: 10.0,
        minorTicksLength: 7.0,
        majorTicksWidth: 0.005,
        minorTicksWidth: 0.0025,
        fillColor: "#333",
        pinColor: "#f88",
        pinRadius: 5.0,
        borderColor: "#000",
        borderWidth: 2.0,
        secondHandColor: "#f00",
        minuteHandColor: "#fff",
        hourHandColor: "#fff",
        fontColor: "#fff",
        fontName: "Tahoma",
        fontSize: 10.0,
        fontWeight: 'normal',
        secondHandLength: 90.0,
        minuteHandLength: 70.0,
        hourHandLength: 50.0,
        secondHandWidth: 1.0,
        minuteHandWidth: 2.0,
        hourHandWidth: 3.0
      };
      htAnalogClock.preset_gray_fantastic = {
        minorTicksLength: 100.0,
        minorTicksColor: "rgba(255, 255, 255, 0.2)",
        majorTicksLength: 100.0,
        majorTicksColor: "rgba(255, 255, 255, 0.6)",
        pinColor: "#aaa",
        pinRadius: 5.0,
        hourHandColor: "#fff",
        hourHandWidth: 5.0,
        minuteHandColor: "#eee",
        minuteHandWidth: 3.0,
        secondHandLength: 95.0
      };
      htAnalogClock.preset_black_bolded = {
        drawBorder: "FALSE",
        majorTicksColor: "rgba(255, 150, 150, 0.8)",
        majorTicksWidth: 0.05,
        drawMinorTicks: "FALSE",
        fillColor: "#000"
      };
      htAnalogClock.preset_white_nice = {
        fillColor: "#fff",
        hourHandColor: "#000",
        minuteHandColor: "#000",
        fontColor: "#333",
        majorTicksColor: "#222",
        minorTicksColor: "#555"
      };
      htAnalogClock.preset_ocean_blue = {
        fillColor: "#4460cb",
        hourHandColor: "#fff",
        minuteHandColor: "#fff",
        fontColor: "#ddd",
        majorTicksColor: "#bbb",
        minorTicksColor: "#aaa",
        fontName: "Sahel FD",
        fontSize: 15.0,
        secondHandColor: "#f80"
      };
      htAnalogClock.preset_nice_bolded = {
        secondHandWidth: 5.0,
        hourHandWidth: 10.0,
        minuteHandWidth: 7.0,
        pinRadius: 10.0,
        pinColor: "#fff",
        fillColor: "#444",
        drawTexts: "FALSE",
        majorTicksWidth: 0.07,
        minorTicksWidth: 0.03,
        majorTicksLength: 50.0,
        minorTicksLength: 25.0,
        majorTicksColor: "rgba(255, 150, 0, 0.6)",
        minorTicksColor: "rgba(0, 150, 250, 0.5)"
      };
      htAnalogClock.preset_modern_dark = {
        majorTicksLength: 50.0,
        minorTicksLength: 50.0,
        majorTicksWidth: 0.02,
        minorTicksWidth: 0.0075,
        fillColor: "#333",
        pinColor: "#000",
        pinRadius: 90.0,
        borderColor: "transparent",
        secondHandColor: "#0f0",
        minuteHandColor: "#fff",
        hourHandColor: "#fff",
        secondHandLength: 100.0,
        minuteHandLength: 100.0,
        hourHandLength: 97.0,
        secondHandWidth: 5.0,
        minuteHandWidth: 3.0,
        hourHandWidth: 10.0
      };
      $(document).ready(function () {
        $(function () {
          $.fn.loop = function (callback, thisArg) {
            var me = this;
            return this.each(function (index, element) {
              return callback.call(thisArg || element, element, index, me);
            });
          };
          $(".adc_block-analog").loop(function (element, index, set) {
            if (jQuery(this)[0].hasAttribute('data-config')) {
              var config = $(this).attr('data-config');
              var obj = JSON.parse(config);
              $(this).htAnalogClock(obj, {
                timezone: obj['timezone']
              });
            }
          });
        });
      });
    }
  }
}(jQuery, Drupal, drupalSettings));
